#!/bin/bash

# SPDX-FileCopyrightText: 2023 ivan tkachenko <me@ratijas.tk>
#
# SPDX-License-Identifier: LGPL-2.0-or-later

# KDE scripts expect certain directory structure. This script sets them up
# first, and cleans up later.

cd "$( dirname "${BASH_SOURCE[0]}" )"

# Unfortunately, existing KDE infra does not allow pushing translation files
# in "standard" po directory, so we have to keep them elsewhere.
typeset podir=translations
typeset tmpdir=tmp

for polangdir in $podir/*; do
  typeset lang=${polangdir##*/}

  # Just having this directory in place makes merge_all.sh happy.
  mkdir -p $tmpdir/$lang/messages
  # Copy actual message catalogues where merge_all.sh expects to find them.
  cp $polangdir/*.po $tmpdir/$lang/
done

(cd $tmpdir && merge_all.sh)

for polangdir in $podir/*; do
  typeset lang=${polangdir##*/}

  cp $tmpdir/$lang/*.po $polangdir/
done

# Clean up
rm -rf $tmpdir
