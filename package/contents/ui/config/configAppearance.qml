/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15
import org.kde.kquickcontrolsaddons 2.1 as KQuickAddons
import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.kirigami 2.20 as Kirigami
import org.kde.plasma.plasmoid 2.0

Kirigami.FormLayout {
    id: root

    property alias cfg_icon: mainIconName.icon.name
    property alias cfg_indicator: indicatorIconName.icon.name
    property int cfg_indicatorIndex: plasmoid.configuration.indicatorIndex
    property alias cfg_getinfo: getinfo.checked
    property alias cfg_notification: notification.checked
    property bool cfg_panel: plasmoid.configuration.panel
    property alias cfg_border: border.checked
    property alias cfg_lastplay: middleLastPlay.checked
    property int iconSize: PlasmaCore.Units.iconSizes.smallMedium
    property alias cfg_speedfactor: slider.speedFactor

    Layout.fillWidth: true

    Item {
        Kirigami.FormData.label: i18n("Behavior")
        Kirigami.FormData.isSection: true
    }

    QQC2.CheckBox {
        id: getinfo
        text: i18n("Get additional info")
        QQC2.ToolTip.visible: hovered
        QQC2.ToolTip.text: i18n("Try to get more info for track using Last-FM API")
    }

    QQC2.CheckBox {
        id: notification
        text: i18n("Show notification")
        QQC2.ToolTip.visible: hovered
        QQC2.ToolTip.text: i18n("Show notification on track change")
    }

    ColumnLayout {
        Kirigami.FormData.label: i18n("Animation speed:")
        Kirigami.FormData.buddyFor: slider

        QQC2.Slider {
            id: slider

            property double speedFactor

            Layout.fillWidth: true

            from: -1.5
            to: 1.5
            stepSize: 0.25
            snapMode: QQC2.Slider.SnapAlways
            value: -(Math.log(speedFactor) / Math.log(2))
            onMoved: {
                speedFactor = 1.0 / Math.pow(2, value)
            }
        }

        RowLayout {
            QQC2.Label {
                text: i18n("Slower")
            }
            Item {
                Layout.fillWidth: true
            }
            QQC2.Label {
                text: i18n("Faster")
            }
        }
    }

    Kirigami.Separator {
        Kirigami.FormData.isSection: true
    }

    Item {
        Kirigami.FormData.label: i18n("Panel settings")
        Kirigami.FormData.isSection: true
    }

    ColumnLayout {
        enabled: plasmoid.formFactor !== PlasmaCore.Types.Planar

        Kirigami.FormData.label: i18n("Panel View:")
        Kirigami.FormData.buddyFor: iconview

        QQC2.RadioButton {
            id: iconview
            text: i18n("Icon")
            checked: !cfg_panel
        }

        QQC2.RadioButton {
            id: panelview
            text: i18n("Track name")
            checked: cfg_panel
            onCheckedChanged: {
                cfg_panel = checked
            }
        }
    }

    Item {
        Kirigami.FormData.isSection: true
    }

    KQuickAddons.IconDialog {
        id: iconDialog

        property var iconObj
        property QQC2.Button button

        iconSize: PlasmaCore.Units.iconSizes.smallMedium //Must specify the size directly - alias don't work
        onIconNameChanged: iconObj.name = iconName
    }

    RowLayout {
        Kirigami.FormData.label: i18n("Plasmoid Icon:")

        visible: !panelview.checked
        enabled: plasmoid.formFactor !== PlasmaCore.Types.Planar

        QQC2.Button {
            id: mainIconName

            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: i18n("Select Icon…")

            icon.width: iconSize
            icon.height: iconSize

            onClicked: {
                iconDialog.button = mainIconName
                iconDialog.open()
                iconDialog.iconObj = iconDialog.button.icon
            }
        }

        QQC2.Button {
            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: i18n("Set Default")

            icon.name: "edit-clear"
            icon.width: iconSize
            icon.height: iconSize

            onClicked: mainIconName.icon.name = "audio-radio"
        }
    }

    RowLayout {
        Kirigami.FormData.label: i18n("Indicator Icon:")

        visible: !panelview.checked
        enabled: plasmoid.formFactor !== PlasmaCore.Types.Planar

        QQC2.Button {
            id: indicatorIconName

            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: i18n("Select Icon…")

            icon.width: iconSize
            icon.height: iconSize
            visible: !panelview.checked

            onClicked: {
                iconDialog.button = indicatorIconName
                iconDialog.open()
                iconDialog.iconObj = iconDialog.button.icon
            }
        }

        QQC2.Button {
            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: i18n("Set Default")

            icon.name: "edit-clear"
            icon.width: iconSize
            icon.height: iconSize

            onClicked: indicatorIconName.icon.name = "media-default-track"
        }
    }

    function openDialog(button) {
        iconDialog.open()
        iconDialog.iconObj = button.icon
    }

    QQC2.Button {
        id: indicator

        Kirigami.FormData.label: i18n("Indicator Position:")

        visible: !panelview.checked
        enabled: plasmoid.formFactor !== PlasmaCore.Types.Planar

        icon.width: iconSize
        icon.height: iconSize
        icon.name: "snap-bounding-box-corners"

        transform: Rotation {
            angle: cfg_indicatorIndex * 90
            origin.x: indicator.width / 2
            origin.y: indicator.height / 2
        }
        onClicked: {
            if (cfg_indicatorIndex === 3) {
                cfg_indicatorIndex = 0
            } else {
                cfg_indicatorIndex += 1
            }
        }
    }

    QQC2.CheckBox {
        id: border

        text: i18n("Show border")
        enabled: plasmoid.formFactor !== PlasmaCore.Types.Planar
        visible: panelview.checked
    }

    Item {
        Kirigami.FormData.isSection: true
    }

    QQC2.CheckBox {
        id: middleLastPlay

        text: i18n("Use middle button")
        enabled: plasmoid.formFactor !== PlasmaCore.Types.Planar

        QQC2.ToolTip.visible: hovered
        QQC2.ToolTip.text: i18n("Use middle click for play last playing station")
    }

    Item {
        Kirigami.FormData.isSection: true
    }
}
