/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *  SPDX-FileCopyrightText: 2023 ivan tkachenko <me@ratijas.tk>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.20 as Kirigami
import QtMultimedia 5.15
import ".." as ARP

FramelessConfigurationPage {
    id: root

    property var items: ["fr", "de", "nl"]
    property string server: ""
    property string cfg_servers: plasmoid.configuration.servers
    property int limit: 500
    property int offset: 0
    property string currentUrl
    property int stat: 1
    property bool isNoSearch: false

    ListModel {
        id: searchModel
        dynamicRoles: true
    }

    ARP.StationsModel {
        id: stationsModel
    }

    Timer {
        id: timerconnect

        repeat: false
        running: true
        interval: 5000
    }

    function getServer() {
        server = items[Math.floor(Math.random() * items.length)]
    }

    function setHeaders(xhr) {
        xhr.setRequestHeader("User-Agent", "AdvancedRadio/2.0")
    }

    function getStations(by, val) {
        const item = server
        const xhr = new XMLHttpRequest
        isNoSearch = !(typeof by !== "undefined" && by !== null)
        offset = 0
        busy.running = true
        busy.visible = true
        gettext.visible = true
        gettext.text = i18n("Get list of stations\nPlease wait…")
        view.enabled = false
        timerconnect.triggered.connect(() => {
            xhr.abort()
            items.splice(items.indexOf(item), 1)
            if (items.length < 1) {
                items = ["fr", "de", "nl"]
            }
            getServer()
            getStations()
        })

        const byVal = isNoSearch ? "" : `/${by}/${val}`
        const url = `https://${item}1.api.radio-browser.info/json/stations${byVal}?hidebroken=true&limit=${limit}&offset=${offset}`

        xhr.open("GET", url)
        setHeaders(xhr)
        xhr.onreadystatechange = () => {
            if (xhr.readyState === xhr.DONE) {
                if (xhr.status === 200) {
                    timerconnect.running = false
                    currentUrl = url.split("?")[0]
                    var servers = JSON.parse(xhr.responseText)
                    searchModel.clear()
                    for (let i = 0; i < servers.length; i++) {
                        searchModel.append(servers[i])
                        searchModel.setProperty(i, "name",
                                                servers[i].name.replace(
                                                    /\n/g, ' ').trim())
                        searchModel.setProperty(i, "added", false)
                    }
                    busy.running = false
                    busy.visible = false
                    gettext.visible = servers.length === 0
                    gettext.text = servers.length === 0
                        ? i18n("Nothing found\nTry changing your query")
                        : i18n("Get list of stations\nPlease wait…")
                    view.enabled = true
                    stat = 1
                }
            }
        }
        xhr.send()
    }

    function loadMore() {
        stat = 0
        const xhr = new XMLHttpRequest
        const baseUrl = currentUrl.split("?")[0]
        const url = `${baseUrl}?hidebroken=true&limit=${limit}&offset=${offset}`
        xhr.open("GET", url)
        setHeaders(xhr)
        xhr.onreadystatechange = () => {
            if (xhr.readyState === xhr.DONE) {
                if (xhr.status === 200) {
                    currentUrl = url
                    const servers = JSON.parse(xhr.responseText)
                    if (servers.length > 0) {
                        for (const srv of servers) {
                            srv.name = srv.name.replace(/\n/g, ' ').trim()
                            srv.added = false
                            searchModel.append(srv)
                        }
                        stat = 1
                    }
                }
            }
        }
        xhr.send()
    }

    Component.onCompleted: {
        stationsModel.clear()
        const servers = JSON.parse(cfg_servers)
        for (const srv of servers) {
            stationsModel.append(srv)
        }
        stat = 0
        getServer()
        getStations()
    }

    ColumnLayout {
        id: view

        Layout.fillHeight: true
        Layout.fillWidth: true

        spacing: 0

        Kirigami.Dialog {
            id: searchDrawer

            title: i18n("Search Station")
            padding: Kirigami.Units.largeSpacing
            standardButtons: Kirigami.Dialog.NoButton

            RowLayout {
                QQC2.Label {
                    text: i18n("Search by")
                }
                QQC2.ComboBox {
                    id: by
                    model: [i18n("name"), i18n("country"), i18n("language"), i18n("tags")]
                }
                Kirigami.SearchField {
                    id: search

                    Layout.fillWidth: true

                    autoAccept: false

                    onAccepted: {
                        if (text !== "") {
                            var filters = ["byname", "bycountry", "bylanguage", "bytag"]
                            var filter = filters[by.currentIndex]
                            testPlay.stop()
                            searchModel.clear()
                            root.currentUrl = ""
                            root.getStations(filter, text)
                            searchDrawer.close()
                        } else {
                            if (!root.isNoSearch) {
                                searchModel.clear()
                                root.getStations()
                                searchDrawer.close()
                            }
                        }
                    }
                }

                QQC2.Button {
                    icon.name: "search"
                    enabled: search.text !== ""
                    onClicked: {
                        search.accepted()
                    }
                }
            }

            onOpened: search.forceActiveFocus(Qt.MouseFocusReason)
        }

        Component {
            id: delegateComponent
            Item {
                id: listItem
                required property int index
                required property var model
                required property string name
                width: ListView.view.width - ListView.view.leftMargin - ListView.view.rightMargin
                height: swipeListItem.height
            Kirigami.SwipeListItem {
                id: swipeListItem
                down: false
                Kirigami.Theme.inherit: false
                Kirigami.Theme.colorSet: Kirigami.Theme.View
                alternatingBackground: true

                contentItem: RowLayout {
                    spacing: Kirigami.Units.smallSpacing

                    Kirigami.Icon {
                        z: 2
                        source: listItem.model.favicon ? listItem.model.favicon : "audio-x-generic"
                        placeholder: "audio-x-generic"
                        fallback: "audio-x-generic"
                    }

                    Item {
                        id: trackRect

                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        clip: true

                        QQC2.Label {
                            id: trackName

                            text: listItem.name.trim().replace(/\n/g, " ")
                            anchors.verticalCenter: parent.verticalCenter
                            color: swipeListItem.textColor

                            XAnimator {
                                target: trackName
                                from: 0
                                to: -trackName.paintedWidth
                                duration: Math.round(
                                              Math.abs(to - from) / Kirigami.Units.gridUnit * 300
                                              * plasmoid.configuration.speedfactor)
                                running: swipeListItem.containsMouse && trackName.width > trackRect.width
                                loops: 1
                                onFinished: {
                                    from = trackRect.width
                                    if (swipeListItem.containsMouse) {
                                        start()
                                    }
                                }
                                onStopped: {
                                    from = 0
                                    trackName.x = 0
                                }
                            }
                        }
                    }

                    Kirigami.Chip {
                        Layout.rightMargin: bitrate.visible ? 0 : Kirigami.Units.largeSpacing
                        text: listItem.model.codec ? listItem.model.codec : ""
                        closable: false
                        enabled: false
                        visible: listItem.model.codec !== "UNKNOWN"
                        implicitWidth: implicitContentWidth
                    }

                    Kirigami.Chip {
                        id: bitrate

                        Layout.rightMargin: Kirigami.Units.largeSpacing

                        text: listItem.model.bitrate ? listItem.model.bitrate + i18n("kBit/s") : ""
                        closable: false
                        enabled: false
                        visible: listItem.model.bitrate !== 0
                        implicitWidth: implicitContentWidth
                    }
                }
                actions: [
                    Kirigami.Action {
                        id: info

                        icon.name: "documentinfo"
                        text: i18n("Info")

                        onTriggered: {
                            listItem.ListView.view.currentIndex = listItem.index
                            if (testPlay.source != searchModel.get(listItem.ListView.view.currentIndex).url_resolved) {
                                testPlay.stop()
                            }
                            message.visible = false
                            infoSheet.open()
                        }
                    },
                    Kirigami.Action {
                        id: addButton

                        text: i18n("Add Station")
                        icon.name: listItem.model.added ? "checkbox" : "list-add"
                        enabled: !listItem.model.added

                        onTriggered: {
                            listItem.ListView.view.currentIndex = listItem.index
                            if (testPlay.source != searchModel.get(listItem.ListView.view.currentIndex).url_resolved) {
                                testPlay.stop()
                            }
                            searchModel.setProperty(listItem.index, "added", true)
                            const itemObject = {
                                name: searchModel.get(listItem.ListView.view.currentIndex).name,
                                hostname: searchModel.get(listItem.ListView.view.currentIndex).url_resolved,
                                active: true,
                            }
                            stationsModel.append(itemObject)
                            cfg_servers = JSON.stringify(getServersArray())
                            if (!message.visible) {
                                message.positive = true
                                message.text = i18n("Station is added. Click 'Apply' to save changes.")
                                message.visible = true
                                closetimer.restart()
                            }
                        }
                    },
                    Kirigami.Action {
                        id: playButton

                        text: (root.isPlaying() && listItem.ListView.view.currentIndex === listItem.index)
                            ? i18n("Stop") : i18n("Play")
                        icon.name: (root.isPlaying() && listItem.ListView.view.currentIndex === listItem.index)
                            ? "media-playback-stop" : "media-playback-start"

                        enabled: listItem.ListView.view.currentIndex !== -1
                            && searchModel.get(listItem.ListView.view.currentIndex).lastcheckok == 1

                        onTriggered: {
                            listItem.ListView.view.currentIndex = listItem.index
                            message.visible = false
                            const currentUrl = searchModel.get(listItem.ListView.view.currentIndex).url_resolved
                            if (root.isPlaying() && testPlay.source == currentUrl) {
                                testPlay.stop()
                                testPlay.source = ""
                            } else {
                                testPlay.stop()
                                testPlay.source = currentUrl
                                testPlay.play()
                            }
                        }
                    }
                ]
            }
        }
        }

        Kirigami.Separator {
            Layout.fillWidth: true
        }

        QQC2.ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            // This is to prevent ColumnLayout from stretching to the whole ListView content height.
            implicitHeight: 0

            ListView {
                id: mainList

                model: searchModel
                moveDisplaced: Transition {
                    YAnimator {
                        duration: Kirigami.Units.longDuration
                        easing.type: Easing.InOutQuad
                    }
                }
                onContentYChanged: {
                    if (contentY > contentHeight - height * 2
                            && root.stat == 1) {
                        root.offset = root.offset + 500
                        loadMore()
                    }
                }
                reuseItems: true
                delegate: delegateComponent

                ColumnLayout {
                    Layout.fillWidth: true
                    anchors.centerIn: parent
                    spacing: 0

                    QQC2.BusyIndicator {
                        id: busy
                        running: false
                        enabled: true
                        implicitWidth: Kirigami.Units.iconSizes.enormous
                        implicitHeight: Kirigami.Units.iconSizes.enormous
                        Layout.alignment: Qt.AlignHCenter
                    }

                    QQC2.Label {
                        id: gettext
                        text: i18n("Get list of stations\nPlease wait…")
                        visible: false
                        enabled: true
                        horizontalAlignment: Text.AlignHCenter
                        Layout.alignment: Qt.AlignHCenter
                    }
                }
            }
        }

        Kirigami.Separator {
            Layout.fillWidth: true
        }

        Kirigami.InlineMessage {
            id: message

            property bool positive

            Layout.fillWidth: true

            visible: false
            showCloseButton: true
            type: positive ? Kirigami.MessageType.Positive : Kirigami.MessageType.Error
        }

        Timer {
            id: closetimer

            running: false
            repeat: false
            interval: 10000
            onTriggered: {
                message.visible = false
            }
        }

        RowLayout {
            Layout.leftMargin: Kirigami.Units.largeSpacing
            Layout.rightMargin: Kirigami.Units.largeSpacing
            Layout.topMargin: Kirigami.Units.largeSpacing

            spacing: Kirigami.Units.smallSpacing

            QQC2.Button {
                text: i18n("Search…")
                icon.name: "search"
                onClicked: searchDrawer.open()
            }

            Item {
                Layout.fillWidth: true
            }

            QQC2.Button {
                text: i18n("Clear Results")
                icon.name: "edit-clear-all"
                enabled: search.text !== ""
                onClicked: {
                    searchModel.clear()
                    search.text = ""
                    search.accepted()
                }
            }
        }

        Kirigami.OverlaySheet {
            id: infoSheet

            header: Kirigami.Heading {
                text: i18n("Station Info")
            }
            contentItem: Kirigami.FormLayout {
                id: formLayout

                wideMode: true

                Kirigami.Heading {
                    Kirigami.FormData.label: i18n("Name:")
                    Layout.maximumWidth: root.width - Kirigami.Units.smallSpacing
                    Layout.preferredWidth: root.width - Kirigami.Units.smallSpacing

                    text: mainList.currentIndex !== -1
                        ? searchModel.get(mainList.currentIndex).name : ""
                    wrapMode: Text.Wrap
                    verticalAlignment: Text.AlignVCenter
                }

                Kirigami.Icon {
                    Kirigami.FormData.label: i18n("Favicon:")
                    Layout.preferredWidth: Kirigami.Units.iconSizes.huge
                    Layout.preferredHeight: Kirigami.Units.iconSizes.huge

                    source: mainList.currentIndex !== -1
                        && searchModel.get(mainList.currentIndex).favicon != ""
                            ? searchModel.get(mainList.currentIndex).favicon
                            : "audio-x-generic"
                    placeholder: "audio-x-generic"
                    fallback: "audio-x-generic"
                }
                Kirigami.UrlButton {
                    Kirigami.FormData.label: i18n("Homepage:")
                    Layout.maximumWidth: root.width

                    url: mainList.currentIndex !== -1
                        ? searchModel.get(mainList.currentIndex).homepage : ""
                    wrapMode: Text.WrapAnywhere
                    horizontalAlignment: Text.AlignLeft
                }

                Kirigami.UrlButton {
                    Kirigami.FormData.label: i18n("Stream URL:")
                    Layout.maximumWidth: root.width

                    url: mainList.currentIndex !== -1
                        ? searchModel.get(mainList.currentIndex).url_resolved : ""
                    wrapMode: Text.WrapAnywhere
                    horizontalAlignment: Text.AlignLeft
                }

                QQC2.Label {
                    property string status: {
                        if (mainList.currentIndex !== -1) {
                            if (searchModel.get(mainList.currentIndex).lastcheckok == 1) {
                                return i18n("OK")
                            } else {
                                return i18n("Error")
                            }
                        } else {
                            return ""
                        }
                    }

                    Kirigami.FormData.label: i18n("Server status:")
                    Layout.maximumWidth: root.width

                    text: {
                        if (mainList.currentIndex !== -1) {
                            const timeModel = searchModel.get(mainList.currentIndex).lastchecktime
                            const timeString = Date.fromLocaleString(locale, timeModel, "yyyy-MM-dd hh:mm:ss").toLocaleString(Qt.locale(), Locale.ShortFormat)
                            const label = i18n("last check: ")
                            return `${status} (${label}${timeString})`
                        } else {
                            return ""
                        }
                    }

                    color: {
                        if (mainList.currentIndex !== -1) {
                            if (searchModel.get(mainList.currentIndex).lastcheckok == 1) {
                                return Kirigami.Theme.positiveTextColor
                            } else {
                                return Kirigami.Theme.negativeTextColor
                            }
                        } else {
                            return Kirigami.Theme.textColor
                        }
                    }
                }

                Kirigami.Chip {
                    Kirigami.FormData.label: i18n("Codec:")

                    text: mainList.currentIndex !== -1
                        ? searchModel.get(mainList.currentIndex).codec : ""
                    closable: false
                    enabled: false
                    visible: mainList.currentIndex !== -1
                        && searchModel.get(mainList.currentIndex).codec != "UNKNOWN"
                }

                Kirigami.Chip {
                    Kirigami.FormData.label: i18n("Bitrate:")

                    text: {
                        if (mainList.currentIndex !== -1) {
                            const bitrate = searchModel.get(mainList.currentIndex).bitrate
                            return bitrate.toString() + i18n("kBit/s")
                        } else {
                            return ""
                        }
                    }
                    closable: false
                    enabled: false
                    visible: mainList.currentIndex !== -1
                        && searchModel.get(mainList.currentIndex).bitrate != 0
                }

                QQC2.Label {
                    Kirigami.FormData.label: i18n("Country:")

                    visible: text !== ""
                    text: mainList.currentIndex !== -1
                        ? searchModel.get(mainList.currentIndex).country : ""
                }

                QQC2.Label {
                    Kirigami.FormData.label: i18n("Language:")

                    visible: text !== ""
                    text: mainList.currentIndex !== -1
                        ? searchModel.get(mainList.currentIndex).language : ""
                }

                Flow {
                    Kirigami.FormData.label: i18n("Tags:")

                    Layout.maximumWidth: root.width
                    Layout.preferredWidth: root.width

                    spacing: Kirigami.Units.smallSpacing
                    visible: mainList.currentIndex !== -1
                        && searchModel.get(mainList.currentIndex).tags.length > 0

                    Repeater {
                        model: mainList.currentIndex !== -1
                            ? searchModel.get(mainList.currentIndex).tags.split(",")
                            : []

                        delegate: Kirigami.Chip {
                            closable: false
                            enabled: false
                            text: modelData
                        }
                    }
                }
            }
        }
    }

    MediaPlayer {
        id: testPlay

        onError: {
            message.positive = false
            message.text = i18n("Error") + ": " + testPlay.errorString
            message.visible = true
            closetimer.restart()
        }
    }

    function checkServers() {
        checkServer.stop()
        checkServer.source = ""
        checkServer.source = searchModel.get(mainList.currentIndex).url_resolved
        checkServer.play()
    }

    function isPlaying() {
        return testPlay.playbackState === MediaPlayer.PlayingState
    }

    function getServersArray() {
        const serversArray = []
        for (let i = 0; i < stationsModel.count; i++) {
            serversArray.push(stationsModel.get(i))
        }
        return serversArray
    }
}
