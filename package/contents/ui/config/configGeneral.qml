
/*
* SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
* SPDX-FileCopyrightText: 2023 ivan tkachenko <me@ratijas.tk>
*
* SPDX-License-Identifier: LGPL-2.0-or-later
*/
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.20 as Kirigami
import Qt.labs.platform 1.1 as Labs
import org.kde.plasma.core 2.1 as PlasmaCore
import ".." as ARP

FramelessConfigurationPage {
    id: root

    property string cfg_servers: plasmoid.configuration.servers
    property int dialogMode: -1

    Component.onCompleted: {
        stationsModel.clear()
        var servers = JSON.parse(cfg_servers)
        for (const server of servers) {
            stationsModel.append(server)
        }
    }

    Component {
        id: delegateComponent
        Item {
            id: listItem
            required property int index
            required property var model

            required property bool active
            required property string name
            width: ListView.view.width - ListView.view.leftMargin - ListView.view.rightMargin
            height: swipeListItem.height
            Kirigami.SwipeListItem {
                id: swipeListItem
                down: false
                alternatingBackground: true
                Kirigami.Theme.inherit: false
                Kirigami.Theme.colorSet: Kirigami.Theme.View
                contentItem: RowLayout {
                    spacing: Kirigami.Units.smallSpacing
                    QQC2.Label {
                        Layout.preferredWidth: listCounterMetrics.advanceWidth
                        horizontalAlignment: Text.AlignHCenter
                        LayoutMirroring.enabled: false
                        text: listItem.index + 1
                        color: swipeListItem.textColor

                        Component.onCompleted: {
                            listCounterMetrics.font = font
                        }
                    }

                    Kirigami.ListItemDragHandle {
                        listItem: swipeListItem
                        listView: listItem.ListView.view
                        onMoveRequested: {
                            stationsModel.move(oldIndex, newIndex, 1)
                            cfg_servers = JSON.stringify(getServersArray())
                        }
                    }

                    Item {
                        id: trackRect

                        clip: true

                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        QQC2.Label {
                            id: trackName

                            anchors.verticalCenter: parent.verticalCenter
                            text: listItem.name
                            color: swipeListItem.textColor

                            XAnimator {
                                target: trackName
                                from: 0
                                to: -trackName.paintedWidth
                                duration: Math.round(
                                              Math.abs(
                                                  to - from) / Kirigami.Units.gridUnit * 300
                                              * plasmoid.configuration.speedfactor)
                                running: swipeListItem.containsMouse
                                         && trackName.width > trackRect.width
                                loops: 1

                                onFinished: {
                                    from = trackRect.width
                                    if (listItem.containsMouse) {
                                        start()
                                    }
                                }
                                onStopped: {
                                    from = 0
                                    trackName.x = 0
                                }
                            }
                        }
                    }
                }
                actions: [
                    Kirigami.Action {
                        icon.name: "edit-entry"
                        text: i18n("Edit")
                        onTriggered: {
                            listItem.ListView.view.currentIndex = listItem.index
                            editServer()
                        }
                    },
                    Kirigami.Action {
                        icon.name: checked ? "view-visible" : "view-hidden"
                        text: checked ? i18n("Hide") : i18n("Show")
                        checked: listItem.active
                        checkable: true
                        onTriggered: {
                            listItem.model.active = checked
                            cfg_servers = JSON.stringify(getServersArray())
                        }
                    },
                    Kirigami.Action {
                        icon.name: "delete"
                        text: i18n("Remove")
                        onTriggered: {
                            stationsModel.remove(listItem.index)
                            cfg_servers = JSON.stringify(getServersArray())
                        }
                    }
                ]
            }
        }
    }

    ARP.StationsModel {
        id: stationsModel
    }

    TextMetrics {
        id: listCounterMetrics
        text: ''.padStart(Math.max(0,
                                   mainList.count - 1).toString().length, '9')
    }

    Kirigami.Separator {
        Layout.fillWidth: true
    }

    QQC2.ScrollView {
        Layout.fillWidth: true
        Layout.fillHeight: true

        // This is to prevent ColumnLayout from stretching to the whole ListView content height.
        implicitHeight: 0

        ListView {
            id: mainList

            model: stationsModel
            moveDisplaced: Transition {
                YAnimator {
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.InOutQuad
                }
            }
            reuseItems: true
            delegate: delegateComponent
        }
    }

    Kirigami.Separator {
        Layout.fillWidth: true
    }

    Kirigami.InlineMessage {
        id: importexportmessage

        property bool positive

        Layout.fillWidth: true

        visible: false
        showCloseButton: true
        type: positive ? Kirigami.MessageType.Positive : Kirigami.MessageType.Error
    }

    Timer {
        id: closetimer

        running: false
        repeat: false
        interval: 10000
        onTriggered: {
            importexportmessage.visible = false
        }
    }

    RowLayout {
        Layout.leftMargin: Kirigami.Units.largeSpacing
        Layout.rightMargin: Kirigami.Units.largeSpacing
        Layout.topMargin: Kirigami.Units.largeSpacing

        spacing: Kirigami.Units.smallSpacing

        QQC2.Button {
            text: i18n("Add…")
            icon.name: "list-add"
            onClicked: addServer()
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.Button {
            text: i18n("Import…")
            icon.name: "document-import"
            onClicked: openFileDialog.open()
        }

        QQC2.Button {
            text: i18n("Export…")
            icon.name: "document-export"
            onClicked: saveFileDialog.open()
        }
    }

    Kirigami.Dialog {
        id: serverDialog

        title: dialogMode === -1 ? i18n("Add Station") : i18n("Edit station")
        padding: Kirigami.Units.largeSpacing
        standardButtons: QQC2.Dialog.Ok | QQC2.Dialog.Cancel

        ColumnLayout {
            Kirigami.FormLayout {
                QQC2.TextField {
                    id: serverName
                    Kirigami.FormData.label: i18n("Name:")
                }

                QQC2.TextField {
                    id: serverHostname
                    Kirigami.FormData.label: i18n("URL:")
                }

                QQC2.CheckBox {
                    id: serverActive
                    checked: true
                    text: i18n("Active")
                }
            }
        }

        onOpened: serverName.forceActiveFocus(Qt.MouseFocusReason)

        onAccepted: {
            const itemObject = {
                "name": serverName.text,
                "hostname": serverHostname.text,
                "active": serverActive.checked
            }

            if (dialogMode === -1) {
                stationsModel.append(itemObject)
            } else {
                stationsModel.set(dialogMode, itemObject)
            }

            cfg_servers = JSON.stringify(getServersArray())
        }
    }

    Labs.FileDialog {
        id: openFileDialog

        nameFilters: ["ARP Stations Backup (*.arp)"]
        fileMode: Labs.FileDialog.OpenFile
        folder: Labs.StandardPaths.writableLocation(
                    Labs.StandardPaths.HomeLocation)

        onAccepted: {
            openFile(currentFile)
        }
    }

    Labs.FileDialog {
        id: saveFileDialog

        nameFilters: ["ARP Stations Backup (*.arp)"]
        folder: Labs.StandardPaths.writableLocation(
                    Labs.StandardPaths.HomeLocation)
        fileMode: Labs.FileDialog.SaveFile

        onVisibleChanged: {
            if (visible) {
                const home = Labs.StandardPaths.writableLocation(
                               Labs.StandardPaths.HomeLocation)
                currentFile = "file: ///" + home + "/stations.arp"
            }
        }
        onAccepted: saveFile(currentFile, cfg_servers)
    }

    function openFile(fileUrl) {
        const request = new XMLHttpRequest()
        request.open("GET", fileUrl, false)
        request.send(null)
        try {
            const servers = JSON.parse(request.responseText)
            console.log(request.responseText)
            stationsModel.clear()
            for (const srv of servers) {
                stationsModel.append(srv)
            }
            cfg_servers = JSON.stringify(getServersArray())
            showMessage(true, i18n(
                            "Сonfiguration has been loaded. Click 'Apply' to save changes."))
        } catch (e) {
            showMessage(false, i18n(
                            "Error loading configuration. Try choosing a different file."))
        }
    }

    function saveFile(fileUrl, content) {
        var file = fileUrl.toString().replace("file:///", "/")
        var xhr = new XMLHttpRequest()
        xhr.open("PUT", file, true)
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8')
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                checkFile(fileUrl)
            } else {
                showMessage(
                    false, i18n(
                        "Error, make sure the selected directory is writable!"))
            }
        }
        xhr.send(content)
    }

    function checkFile(fileUrl) {
        var request = new XMLHttpRequest()
        request.open("GET", fileUrl, true)
        request.onreadystatechange = () => {
            if (request.readyState === 4 && request.status === 200) {
                if (cfg_servers === request.responseText) {
                    showMessage(true,
                                i18n("You сonfiguration saved successfully."))
                } else {
                    showMessage(
                        false, i18n(
                            "Error, make sure the selected directory is writable!"))
                }
            }
        }
        request.send(null)
    }

    function showMessage(positive, text) {
        importexportmessage.positive = positive
        importexportmessage.text = text
        importexportmessage.visible = true
        closetimer.restart()
    }

    function getServersArray() {
        var serversArray = []
        for (var i = 0; i < stationsModel.count; i++) {
            serversArray.push(stationsModel.get(i))
        }
        return serversArray
    }

    function addServer() {
        dialogMode = -1
        serverName.text = ""
        serverHostname.text = ""
        serverActive.checked = true
        serverDialog.visible = true
        serverName.focus = true
    }

    function editServer() {
        dialogMode = mainList.currentIndex
        serverName.text = stationsModel.get(dialogMode).name
        serverHostname.text = stationsModel.get(dialogMode).hostname
        serverActive.checked = stationsModel.get(dialogMode).active
        serverDialog.visible = true
        serverName.focus = true
    }
}
