/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15
import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.plasmoid 2.0

PlasmaExtras.Representation {
    id: fullRepresentation

    Layout.minimumWidth: PlasmaCore.Units.gridUnit * 14
    Layout.minimumHeight: PlasmaCore.Units.gridUnit * 18
    Layout.maximumWidth: PlasmaCore.Units.gridUnit * 56
    Layout.maximumHeight: PlasmaCore.Units.gridUnit * 36
    Layout.preferredHeight: Layout.minimumHeight

    collapseMarginsHint: true

    readonly property var appletInterface: Plasmoid.self

    header: PlasmaExtras.PlasmoidHeading {
        id: header

        focus: true
        visible: !Plasmoid.configuration.panel || Plasmoid.formFactor === PlasmaCore.Types.Planar
        // hide for shadow background on desktop
        background.visible: Plasmoid.userBackgroundHints !== PlasmaCore.Types.ShadowBackground

        Heading {}
    }

    PlasmaComponents3.ScrollView {
        id: scrollView

        anchors.fill: parent
        topPadding: PlasmaCore.Units.smallSpacing
        bottomPadding: PlasmaCore.Units.smallSpacing

        PlasmaComponents3.ScrollBar.horizontal.policy: PlasmaComponents3.ScrollBar.AlwaysOff
        PlasmaComponents3.ScrollBar.vertical.policy: Plasmoid.userBackgroundHints !== PlasmaCore.Types.ShadowBackground || Plasmoid.formFactor !== PlasmaCore.Types.Planar
            ? PlasmaComponents3.ScrollBar.AsNeeded : PlasmaComponents3.ScrollBar.AlwaysOff

        contentWidth: availableWidth - contentItem.leftMargin - contentItem.rightMargin
        contentItem: ListView {
            id: stationView

            leftMargin: PlasmaCore.Units.smallSpacing
            rightMargin: PlasmaCore.Units.smallSpacing
            model: stationsModel
            enabled: isConnected
            focus: true
            currentIndex: 0
            boundsBehavior: Flickable.StopAtBounds
            clip: true
            delegate: MediaListItem {}

            Connections {
                target: playMusic
                function onError() {
                    stationView.currentIndex = -1
                }
            }
        }
    }

    footer: PlasmaExtras.PlasmoidHeading {
        background.visible: Plasmoid.userBackgroundHints !== PlasmaCore.Types.ShadowBackground

        RowLayout {
            Rectangle {
                implicitWidth: fullRepresentation.width
                width: fullRepresentation.width

                Layout.topMargin: PlasmaCore.Units.smallSpacing
                Layout.bottomMargin: PlasmaCore.Units.smallSpacing
                Layout.preferredHeight: childrenRect.height

                color: "transparent"

                PlasmaComponents3.Label {
                    id: subtext

                    width: parent.width
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    font: PlasmaCore.Theme.smallestFont
                    elide: Text.ElideRight

                    clip: true
                    color: {
                        if (isError || !isConnected) {
                            return PlasmaCore.ColorScope.negativeTextColor
                        } else if (Plasmoid.userBackgroundHints === PlasmaCore.Types.ShadowBackground) {
                            return PlasmaCore.ColorScope.highlightedTextColor
                        } else {
                            return PlasmaCore.ColorScope.textColor
                        }
                    }
                    text: {
                        if (!isConnected) {
                            return i18n("Check internet connection…")
                        } else if (root.isError) {
                            return i18n("Error: ") + playMusic.errorString
                        } else if (isPlaying() && playMusic.status === MediaPlayer.Buffered) {
                            return i18n("Bitrate: ") + Math.round(playMusic.metaData.audioBitRate / 1000) + 'Kb/s, ' + i18n("Genre: ") + playMusic.metaData.genre
                        } else if (playMusic.bufferProgress < 1 && playMusic.status !== 1) {
                            return i18n("Buffering…") + ' ' + Math.round(playMusic.bufferProgress * 100) + "%"
                        } else {
                            return i18n("Choose station and enjoy…")
                        }
                    }
                }
            }
        }
    }
}
