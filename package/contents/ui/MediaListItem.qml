/*
 *  SPDX-FileCopyrightText: 2022-2023 Yuri Saurov <dr@i-glu4it.ru>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15
import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.plasmoid 2.0

PlasmaComponents3.ItemDelegate {
    id: listItem

    property bool isTry: false

    width: parent ? parent.width : 0
    clip: true

    background: PlasmaCore.FrameSvgItem {
        imagePath: "widgets/viewitem"
        prefix: (stationView.currentIndex === model.index && isPlaying()
                 || listItem.hovered ? "hover" : "normal")
        anchors.fill: parent
        opacity: stationView.currentIndex === model.index && isPlaying()
                 && playMusic.status === MediaPlayer.Buffered ? 1 : 0.5
    }

    contentItem: ColumnLayout {
        spacing: infoLayout.visible ? PlasmaCore.Units.smallSpacing : 0

        RowLayout {
            id: listItemLayout

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft
            implicitHeight: PlasmaCore.Units.gridUnit + PlasmaCore.Units.smallSpacing

            clip: true

            PlasmaCore.IconItem {
                id: listIcon

                colorGroup: PlasmaCore.ColorScope.colorGroup
                source: expandToggleButton.hovered && !infoLayout.expanded
                    ? "documentinfo"
                    : stationView.currentIndex === model.index && listItem.hovered && isPlaying()
                        ? "media-playback-stop"
                        : "media-playback-start"
                implicitWidth: PlasmaCore.Units.iconSizes.sizeForLabels
                implicitHeight: PlasmaCore.Units.iconSizes.sizeForLabels
                visible: !tryToPlayIndicator.visible && listItem.hovered
                         || stationView.currentIndex === model.index
                         && playMusic.bufferProgress === 1
            }
            PlasmaComponents3.Label {
                Layout.minimumWidth: PlasmaCore.Units.iconSizes.sizeForLabels
                Layout.preferredHeight: PlasmaCore.Units.iconSizes.sizeForLabels

                width: height
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter

                text: model.index + 1
                visible: !listIcon.visible && !tryToPlayIndicator.visible
            }

            PlasmaComponents3.BusyIndicator {
                id: tryToPlayIndicator

                running: visible
                visible: isPlaying() && stationView.currentIndex === model.index && playMusic.bufferProgress < 1
                implicitWidth: PlasmaCore.Units.iconSizes.sizeForLabels
                implicitHeight: PlasmaCore.Units.iconSizes.sizeForLabels
            }

            Rectangle {
                id: trackRect

                Layout.fillWidth: true
                height: parent.height

                clip: true
                color: "transparent"

                PlasmaComponents3.Label {
                    id: trackName

                    Layout.alignment: Qt.AlignLeft
                    Layout.fillWidth: true
                    anchors.verticalCenter: parent.verticalCenter

                    text: model.name
                    font.weight: stationView.currentIndex === model.index && isPlaying() && playMusic.status === MediaPlayer.Buffered
                        ? Font.Bold : Font.Normal
                    maximumLineCount: 1
                    elide: Text.ElideRight

                    XAnimator {
                        target: trackName
                        from: 0
                        to: -trackName.width
                        duration: Math.round(
                                      Math.abs(to - from) / PlasmaCore.Units.gridUnit
                                      * 300 * Plasmoid.configuration.speedfactor)
                        running: listItem.hovered && trackName.width > trackRect.width
                        loops: 1
                        onFinished: {
                            from = trackRect.width
                            if (listItem.hovered) {
                                start()
                            }
                        }
                        onStopped: {
                            from = 0
                            trackName.x = 0
                        }
                    }
                }
            }

            PlasmaComponents3.ToolButton {
                id: expandToggleButton

                implicitWidth: PlasmaCore.Units.iconSizes.sizeForLabels
                implicitHeight: PlasmaCore.Units.iconSizes.sizeForLabels

                visible: stationView.currentIndex === model.index && root.artisturl !== "" && root.songurl !== ""
                display: PlasmaComponents3.AbstractButton.IconOnly
                icon.name: infoLayout.expanded ? "collapse" : "expand"

                onClicked: infoLayout.toggleExpanded()
            }

            TapHandler {
                target: listItemLayout

                onTapped: {
                    isError = false
                    errorTimer.stop()
                    stationView.currentIndex = model.index
                    refreshServer(model.index)
                    lastPlay = model.index
                    infoLayout.expanded = false
                }
            }
        }

        ColumnLayout {
            id: infoLayout

            property bool expanded: false

            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.margins: 0
            Layout.preferredWidth: parent.width
            width: parent.width

            spacing: PlasmaCore.Units.smallSpacing

            clip: true
            visible: stationView.currentIndex === model.index && expanded && root.artist !== ""
            opacity: expanded ? 1 : 0

            Behavior on opacity {
                NumberAnimation {
                    duration: PlasmaCore.Units.veryLongDuration
                    easing.type: Easing.InOutCubic
                }
            }

            Image {
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                source: root.albumimage
            }

            PlasmaComponents3.ToolButton {
                Layout.alignment: Qt.AlignHCenter
                Layout.maximumWidth: parent.width

                icon.name: "view-media-artist"
                text: root.artist
                visible: text !== ""

                onClicked: Qt.openUrlExternally(root.artisturl)
            }

            PlasmaComponents3.ToolButton {
                Layout.alignment: Qt.AlignHCenter
                Layout.maximumWidth: parent.width

                icon.name: "view-media-track"
                text: root.song
                visible: text !== ""

                onClicked: Qt.openUrlExternally(root.songurl)
            }

            PlasmaComponents3.ToolButton {
                Layout.alignment: Qt.AlignHCenter
                Layout.maximumWidth: parent.width

                icon.name: "view-media-album-cover"
                text: root.album
                visible: text !== ""

                onClicked: Qt.openUrlExternally(root.albumurl)
            }

            function toggleExpanded() {
                expanded = !expanded
            }
        }
    }
}
