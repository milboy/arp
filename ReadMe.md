# Advanced Radio Player

Radio Player with editable list of stations

## Screenshots

![Plasmoid full view](screenshots/preview.png)
![Plasmoid full view](screenshots/preview2.png)
![Plasmoid full view](screenshots/preview3.png)
![Plasmoid full view](screenshots/preview4.png)
![Plasmoid full view](screenshots/preview5.png)
![Plasmoid full view](screenshots/preview6.png)

## Features

* Add/delete/edit stations in the list
* Display album art (if possible) and more info for track
* Search for radio stations

## Requirements

* [Gstreamer and gstreamer-plugins](https://gstreamer.freedesktop.org/documentation/installing/on-linux.html?gi-language=c)
* Qt >= 5.15
* QtQuickControls >= 2.15
* KDE Frameworks >= 5.95

## Installation

The preferred and easiest way to install is to use Plasma Discover or KDE Get New Stuff and search for *Advanced Radio Player*.

## Develop

Clone this repository locally somewhere. Symlink `package` directory to `~/.local/share/plasma/plasmoids/org.kde.plasma.advancedradio` as follow:

```sh
cd ~/projects  # example directory for sources
git clone https://invent.kde.org/saurov/arp
cd arp
mkdir ~/.local/share/plasma/plasmoids/
cd ~/.local/share/plasma/plasmoids/
ln -s ~/projects/arp/package org.kde.plasma.advancedradio
```

Now you can run it with `plasmoidviewer -a org.kde.plasma.advancedradio`. Note: usually `plasmoidviewer` utility comes with plasma-sdk package from your distro.

### Localization

Make sure you understand the content of this page first: https://techbase.kde.org/Development/Tutorials/Localization/i18n_Build_Systems

In order to build and test translations, you need to clone KDE repository `l10n-scripty` that handles translations, and add it to your `$PATH`:

```sh
cd ~/projects
git clone https://invent.kde.org/sysadmin/l10n-scripty
export PATH=$PATH:$HOME/projects/l10n-scripty
```

In order to make `$PATH` changes persistent, add it to `~/.bashrc`, `~/.zshrc` or your shell of choice.

After editing some strings in the QML source code, you need to extract them into a template and merge with existing \*.po message catalogs. Template file \*.pot is temporary, and not to be committed.

```sh
./extract.sh && ./merge.sh
```

Test your changes by compiling and installing message catalogs:

```sh
./build.sh
plasmoidviewer -a org.kde.plasma.advancedradio
```

If you are a translator, you can either

- work on an existing \*.po message catalog in the `translations` directory by editing it in Qt Linguist or any other editor, or
- to add a new locale, create a new directory under `translations` with the name of your locale code, generate template catalog with `./extract.sh` as shown above, and move that generated \*.pot file from `tmp/templates/` to the folder you created earlied in this step. Then you can open it in Qt Linguist, set locale code there too, and proceed with translating as normal.

## Credits

### [Radio Browser](https://www.radio-browser.info) (API for search stations)

### [LastFM](https://www.last.fm) (Track recognition API)
